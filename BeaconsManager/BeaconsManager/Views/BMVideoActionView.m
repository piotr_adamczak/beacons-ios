//
//  BMVideoActionView.m
//  BeaconsManager
//
//  Created by Piotrek on 17.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMVideoActionView.h"
#import <QuartzCore/QuartzCore.h>

@interface BMVideoActionView()
@end

@implementation BMVideoActionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.videoPlaceholder = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.videoPlaceholder setContentMode:UIViewContentModeScaleAspectFit];
        [self.videoPlaceholder setBackgroundColor:[UIColor blackColor]];
        [self addSubview:self.videoPlaceholder];
    }
    return self;
}

- (void)setMediaUrl:(NSURL *)url {
    
    self.player = [[MPMoviePlayerController alloc] initWithContentURL:url];
    self.player.controlStyle = MPMovieControlStyleDefault;
    [self.player prepareToPlay];

    [self addSubview:self.player.view];
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect imageFrame = CGRectMake(0, 10 , CGRectGetWidth(self.bounds), 180);
    [self.videoPlaceholder setFrame:imageFrame];
    [self.player.view setFrame:imageFrame];
    
    CGSize textSize = [self textSize];
    CGRect textFrame = CGRectMake(10, CGRectGetMaxY(imageFrame)+ 10, textSize.width, textSize.height);
    [self.textView setFrame:textFrame];
}

- (void)stopMedia {
    [self.player stop];
}

@end

//
//  BMActionView.h
//  BeaconsManager
//
//  Created by Piotrek on 17.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BMActionView : UIView

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UITextView * textView;
@property (nonatomic, strong) UIButton * closeButton;

- (void)setTitle:(NSString*)title;
- (void)setText:(NSString*)text;
- (void)setMediaUrl:(NSURL*)url;

- (void)stopMedia;

- (CGSize)textSize;
- (void)setVisible:(BOOL)visible completion:(void (^)(BOOL finished))completion;

@end

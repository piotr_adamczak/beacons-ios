//
//  BMImageActionView.m
//  BeaconsManager
//
//  Created by Piotrek on 17.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMImageActionView.h"

#import <AFNetworking/UIImageView+AFNetworking.h>

@interface BMImageActionView()
@property (nonatomic, strong) UIImageView * imageView;
@end

@implementation BMImageActionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [self.imageView setBackgroundColor:[UIColor blackColor]];
        [self addSubview:self.imageView];
    }
    return self;
}

- (void)setMediaUrl:(NSURL *)url {
    [self.imageView setImageWithURL:url];
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect imageFrame = CGRectMake(0, 10, CGRectGetWidth(self.bounds), 180);
    [self.imageView setFrame:imageFrame];
    
    CGSize textSize = [self textSize];
    CGRect textFrame = CGRectMake(10, CGRectGetMaxY(imageFrame)+ 10, textSize.width, textSize.height);
    [self.textView setFrame:textFrame];
}

@end

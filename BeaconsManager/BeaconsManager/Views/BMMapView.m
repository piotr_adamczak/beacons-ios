//
//  BMMapView.m
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMMapView.h"

#import <AFNetworking/UIImageView+AFNetworking.h>

//Models
#import "BMBeaconPlacement.h"
#import "BMMapAction.h"

@interface BMMapView()
@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic, strong) NSArray * beaconPlacements;
@property (nonatomic, strong) NSArray * mapActions;
@property (nonatomic, strong) NSArray * beaconsImageViews;
@end

@implementation BMMapView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
        self.delegate = self;
      
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self addSubview:self.imageView];
        
        self.currentPositionView = [[UIView alloc] initWithFrame:CGRectZero];
        self.currentPositionView.alpha = 0.70;
        self.currentPositionView.layer.cornerRadius = 20;
        self.currentPositionView.backgroundColor = [UIColor redColor];
        
        self.helperPositionView = [[UIView alloc] initWithFrame:CGRectZero];
        self.helperPositionView.alpha = 0.40;
        self.helperPositionView.layer.cornerRadius = 20;
        self.helperPositionView.backgroundColor = [UIColor grayColor];
        
    }
    return self;
}

- (void)setImageURL:(NSURL *)imageURL {
    
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:imageURL];
    
    [self.imageView setImageWithURLRequest:urlRequest placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [self setImage:image];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = [self centeredFrameForScrollView:self andUIView:self.imageView];
}

- (void)setImage:(UIImage*)image {
    [self.imageView setImage:image];
    [self.imageView setFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    self.imageView.frame = [self centeredFrameForScrollView:self andUIView:self.imageView];
    
    self.contentSize = image.size;
    self.minimumZoomScale = CGRectGetWidth([[UIScreen mainScreen] bounds]) / self.imageView.frame.size.width;
    self.maximumZoomScale = 2.0;
    
    [self setZoomScale:self.minimumZoomScale];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    self.imageView.frame = [self centeredFrameForScrollView:scrollView andUIView:self.imageView];
}

- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
    
    if (scroll.bounds.size.width == 0) return CGRectZero;
    
    CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
    
    return frameToCenter;
}

- (void)updateCurrentPosition:(CGPoint)currentPosition {

    if ([self.currentPositionView superview] == nil) {
        [self.imageView addSubview:self.currentPositionView];
    }
    
    CGFloat proximity = 20;

    CGFloat x = currentPosition.x / 2.0;
    CGFloat y = currentPosition.y / 2.0;
    CGRect frame = CGRectMake(x, y, proximity, proximity);

    [UIView animateWithDuration:0.3 animations:^{
        self.currentPositionView.layer.cornerRadius = proximity / 2.0;
        [self.currentPositionView setFrame:frame];
    }];
}

- (void)hideHelper {
    self.helperPositionView.alpha = 0;
}

- (void)setShowRealRadius:(BOOL)showRealRadius {
    _showRealRadius = showRealRadius;
    self.helperPositionView.alpha = showRealRadius ? 0.4 :0;
}

- (void)updateHelperPosition:(CGPoint)currentPosition {
    
    if ([self.helperPositionView superview] == nil) {
        [self.imageView addSubview:self.helperPositionView];
    }
    
    CGFloat proximity = 20;
    
    CGFloat x = currentPosition.x / 2.0;
    CGFloat y = currentPosition.y / 2.0;
    CGRect frame = CGRectMake(x, y, proximity, proximity);
    
    [UIView animateWithDuration:0.3 animations:^{
        self.helperPositionView.layer.cornerRadius = proximity / 2.0;
        [self.helperPositionView setFrame:frame];
    }];
}

- (void)setBeaconPlacements:(NSArray *)beaconPlacements {
    
    _beaconPlacements = beaconPlacements;
    
    for (UIView * beaconView in self.beaconsImageViews) {
        [beaconView removeFromSuperview];
    }
    
    NSMutableArray * beaconsImageViews = [[NSMutableArray alloc] init];
    
    for (BMBeaconPlacement * beaconPlacement in beaconPlacements) {
        
        CGPoint center = [beaconPlacement centerForImage];
        center.x -= 17;
        center.y -= 22;
        
        CGFloat x = center.x / 2.0;
        CGFloat y = center.y / 2.0;
        
        UIImageView * testView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_beacon_green"]];
        
        CGSize imageSize = testView.image.size;
        CGRect frame = CGRectMake(x, y, imageSize.width, imageSize.height);
        [testView setFrame:frame];
        [self.imageView addSubview:testView];
        
        if (self.showRealRadius) {
            
            if ([beaconPlacement.realRadius floatValue] <= 2.3f && self.showRadiusOnlyWhenCloserThan2Meters) {
                CAShapeLayer * circleShape = [self circleLayerWithRadius:[beaconPlacement.radius floatValue]];
                [testView.layer addSublayer:circleShape];
                
            } else if (!self.showRadiusOnlyWhenCloserThan2Meters) {
                CAShapeLayer * circleShape = [self circleLayerWithRadius:[beaconPlacement.radius floatValue]];
                [testView.layer addSublayer:circleShape];
            }
        }
        
        [beaconsImageViews addObject:testView];
    }
    
    self.beaconsImageViews = beaconsImageViews;
}

- (CAShapeLayer*)circleLayerWithRadius:(CGFloat)radius {
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path addArcWithCenter:CGPointMake(7.5, 11)
                    radius:radius
                startAngle:0.0
                  endAngle:M_PI * 2.0
                 clockwise:YES];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor blueColor] CGColor];
    shapeLayer.fillColor = [UIColor colorWithRed:0 green:0 blue:255 alpha:0.05].CGColor;
    shapeLayer.lineWidth = 1.0;
    return shapeLayer;
}

- (void)setMapAcitons:(NSArray*)mapActions target:(id)target {
    _mapActions = mapActions;
    
    for (BMMapAction * mapAction in mapActions) {
        
        CGPoint center = [mapAction centerForImage];
        CGFloat imageProximity = [mapAction imageProximity];
        
        CGFloat x = center.x / 2.0;
        CGFloat y = center.y / 2.0;
        
        UIView * testView = [[UIView alloc] initWithFrame:CGRectZero];
        testView.alpha = 0.20;
        testView.layer.cornerRadius = imageProximity / 2.0;
        testView.backgroundColor = [UIColor blueColor];
        CGRect frame = CGRectMake(x, y, imageProximity, imageProximity);
        [testView setFrame:frame];
        [testView setTag:[mapAction.uid integerValue]];
        [self.imageView addSubview:testView];
        [self.imageView setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer * gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:target action:@selector(mapActionTapped:)];
        [testView addGestureRecognizer:gestureRecognizer];
    }
}

- (void)setDebugEnabled:(BOOL)debugEnabled {
    [self setShowRealRadius:debugEnabled];
    [self setBeaconPlacements:self.beaconPlacements];
}

@end

//
//  BMVideoActionView.h
//  BeaconsManager
//
//  Created by Piotrek on 17.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMActionView.h"
#import <MediaPlayer/MediaPlayer.h>

@interface BMVideoActionView : BMActionView
@property (nonatomic, strong) UIImageView * videoPlaceholder;
@property (nonatomic, strong) MPMoviePlayerController * player;
@end

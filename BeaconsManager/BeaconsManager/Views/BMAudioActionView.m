//
//  BMAudioActionView.m
//  BeaconsManager
//
//  Created by Piotrek on 18.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMAudioActionView.h"

@implementation BMAudioActionView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect imageFrame = CGRectMake(0, 10, CGRectGetWidth(self.bounds), 39);
    [self.videoPlaceholder setFrame:imageFrame];
    [self.player.view setFrame:imageFrame];
    
    CGSize textSize = [self textSize];
    CGRect textFrame = CGRectMake(10, CGRectGetMaxY(imageFrame)+ 10, textSize.width, textSize.height);
    [self.textView setFrame:textFrame];
}

@end

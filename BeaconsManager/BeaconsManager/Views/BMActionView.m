//
//  BMActionView.m
//  BeaconsManager
//
//  Created by Piotrek on 17.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMActionView.h"

#import <QuartzCore/QuartzCore.h>

@interface BMActionView()
@end

@implementation BMActionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.9];
        
        [self setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
        
        self.textView = [[UITextView alloc] initWithFrame:CGRectZero];
        [self.textView setTextColor:[UIColor blackColor]];
        [self.textView setEditable:NO];
        [self.textView setBackgroundColor:[UIColor clearColor]];
        [self.textView setFont:[UIFont systemFontOfSize:14]];
        [self addSubview:self.textView];
    }
    return self;
}

- (void)setTitle:(NSString*)title {
    [self.titleLabel setText:title];
    [self setNeedsLayout];
}

- (void)setText:(NSString *)text {
    [self.textView setText:text];
    [self setNeedsLayout];
}

- (void)setMediaUrl:(NSURL *)url {
    
}

- (CGSize)textSize {
    
    CGSize maxSize = CGSizeMake(CGRectGetWidth(self.bounds)-20, CGFLOAT_MAX);
    return [self.textView sizeThatFits:maxSize];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize textSize = [self textSize];
    CGRect textFrame = CGRectMake(10, 10, textSize.width, textSize.height);
    [self.textView setFrame:textFrame];
    
    [self bringSubviewToFront:self.closeButton];
}

-(void)setVisible:(BOOL)visible completion:(void (^)(BOOL finished))completion {
    
    [self setNeedsDisplay];
    
    self.alpha = visible ? 0 : 1;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = visible ? 1 : 0;
    } completion:completion];
}

- (void)stopMedia {
    
}

@end

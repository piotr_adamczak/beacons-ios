//
//  BMMapView.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <UIKit/UIKit.h>

//Models
#import "BMBeaconPlacement.h"

@interface BMMapView : UIScrollView<UIScrollViewDelegate>

@property (nonatomic, strong) UIView * currentPositionView;
@property (nonatomic, strong) UIView * helperPositionView;
@property (nonatomic, assign) BOOL showRealRadius;
@property (nonatomic, assign) BOOL showRadiusOnlyWhenCloserThan2Meters;

- (void)setImageURL:(NSURL*)imageURL;
- (void)setBeaconPlacements:(NSArray*)beaconPlacements;
- (void)setMapAcitons:(NSArray*)mapActions target:(id)target;
- (void)updateCurrentPosition:(CGPoint)currentPosition;
- (void)updateHelperPosition:(CGPoint)currentPosition;
- (void)hideHelper;
- (void)setDebugEnabled:(BOOL)debugEnabled;

@end

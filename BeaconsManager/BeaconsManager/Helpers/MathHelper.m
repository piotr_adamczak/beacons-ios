//
//  MathHelper.m
//  BeaconsManager
//
//  Created by Piotrek on 26.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "MathHelper.h"

@implementation MathHelper

+ (CGPoint)addPoint:(CGPoint)pointA toPoint:(CGPoint)pointB {
    return CGPointMake(pointA.x + pointB.x, pointA.y + pointB.y);
}

+ (CGPoint)substractPoint:(CGPoint)pointA withPoint:(CGPoint)pointB {
    return CGPointMake(pointA.x - pointB.x, pointA.y - pointB.y);
}

+ (CGPoint)multiplyPoint:(CGPoint)pointA withValue:(CGFloat)multiplier {
    return CGPointMake(pointA.x * multiplier, pointA.y * multiplier);
}

+ (CGFloat)lengthBetweenPoints:(CGPoint)pointA withPoint:(CGPoint)pointB {
    CGFloat a = pointA.x - pointB.x;
    CGFloat b = pointA.y - pointB.y;
    return sqrtf(a*a + b*b);
}

@end

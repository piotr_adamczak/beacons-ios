//
//  MathHelper.h
//  BeaconsManager
//
//  Created by Piotrek on 26.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MathHelper : NSObject

+ (CGPoint)addPoint:(CGPoint)pointA toPoint:(CGPoint)pointB;
+ (CGPoint)substractPoint:(CGPoint)pointA withPoint:(CGPoint)pointB;
+ (CGFloat)lengthBetweenPoints:(CGPoint)pointA withPoint:(CGPoint)pointB;
+ (CGPoint)multiplyPoint:(CGPoint)pointA withValue:(CGFloat)multiplier;

@end

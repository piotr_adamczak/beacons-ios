//
//  NewBeaconsViewController.m
//  BeaconsManager
//
//  Created by Piotrek on 16.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "NewBeaconsViewController.h"

//Cells
#import "BeaconCell.h"

//Models
#import "RealBeacon.h"
#import "BMBeacon.h"

//Managers
#import "BMBeaconManager.h"

@interface NewBeaconsViewController ()
@property (nonatomic, strong) NSArray * beacons;
@property (nonatomic, strong) ESTBeaconManager* beaconManager;
@property (nonatomic, strong) NSMutableArray * discoverBeacons;
@end

@implementation NewBeaconsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:NSLocalizedString(@"Nearby", nil)];
    
    self.beaconManager = [BMBeaconManager standardBeaconManagerWithDelegate:self];
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.beacons.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 96.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * beaconIdentifier = @"beaconIdentifier";
    
    BeaconCell *cell = [tableView dequeueReusableCellWithIdentifier:beaconIdentifier];
    if (!cell) {
        cell = [[BeaconCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:beaconIdentifier];
    }
    
    ESTBeacon * beacon = [self beaconForIndexPath:indexPath];
    [cell setMinor:[[beacon minor] stringValue]];
    [cell setMajor:[[beacon major] stringValue]];
    [cell setIdentifier:[[beacon proximityUUID] UUIDString]];
    [cell setProximity:[NSString stringWithFormat:@"%.2f m", [[beacon distance] floatValue]]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ESTBeacon * beacon = [self beaconForIndexPath:indexPath];
    [[BMBeaconManager sharedBMBeaconManager] postBeacon:beacon block:^(BOOL success) {
        
        if (success) {
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Success", nil)
                                                             message:@"Beacon added successfully"
                                                            delegate:nil cancelButtonTitle:nil
                                                   otherButtonTitles:@"Great!", nil];
            [alertView show];
            
            [BMBeaconManager fetchBeaconsWithBlock:^(BOOL success, NSArray *beacons) {
                NSSortDescriptor * uuidDesc = [NSSortDescriptor sortDescriptorWithKey:@"uuid" ascending:YES];
                NSSortDescriptor * majorDesc = [NSSortDescriptor sortDescriptorWithKey:@"major" ascending:YES];
                NSSortDescriptor * minorDesc = [NSSortDescriptor sortDescriptorWithKey:@"minor" ascending:YES];
                
                self.alreadyAddedBeacons = [beacons sortedArrayUsingDescriptors:@[uuidDesc, majorDesc, minorDesc]];
                [self.tableView reloadData];
            }];
        }
    }];
}

- (ESTBeacon*)beaconForIndexPath:(NSIndexPath*)indexPath {
    if (indexPath.row < self.beacons.count) {
        return [self.beacons objectAtIndex:indexPath.row];
    }
    return nil;
}

#pragma mark - Beacons 

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region {
    
    NSMutableArray * realBeacons = [[NSMutableArray alloc] initWithArray:beacons];
    
    for (BMBeacon * apiBeacon in self.alreadyAddedBeacons) {
        
        NSUUID * uuid = [[NSUUID alloc] initWithUUIDString:apiBeacon.uuid];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"proximityUUID == %@ AND major == %@ AND minor == %@", uuid, apiBeacon.major, apiBeacon.minor];
        
         NSArray * filteredBeacons = [realBeacons filteredArrayUsingPredicate:predicate];
        
        for (ESTBeacon * realBeacon in filteredBeacons) {
            [realBeacons removeObject:realBeacon];
        }
    }
    
    NSSortDescriptor * descriptior = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    self.beacons = [realBeacons sortedArrayUsingDescriptors:@[descriptior]];
    [self.tableView reloadData];
}

@end

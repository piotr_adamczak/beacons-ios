//
//  BeaconsViewController.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BeaconsViewController.h"
#import "NewBeaconsViewController.h"

//Models
#import "BMBeacon.h"

//Manager
#import "BMBeaconManager.h"
#import "BMFileManager.h"

//Cells
#import "BeaconCell.h"

//Clients
#import "BMBeaconsClient.h"

@interface BeaconsViewController ()
@property (nonatomic, strong) NSArray * beacons;
@property (nonatomic, strong) NSArray * realBeacons;
@property (nonatomic, strong) ESTBeaconManager* beaconManager;

@end

@implementation BeaconsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:NSLocalizedString(@"Beacons", nil)];
    
    self.beaconManager = [BMBeaconManager standardBeaconManagerWithDelegate:self];
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];

    [self loadData];
    
    [BMFileManager writeAtEndOfFile:@"\nNEW SESSION\n"];
}

- (void)loadData {
    [BMBeaconManager fetchBeaconsWithBlock:^(BOOL success, NSArray *beacons) {
        NSSortDescriptor * uuidDesc = [NSSortDescriptor sortDescriptorWithKey:@"uuid" ascending:YES];
        NSSortDescriptor * majorDesc = [NSSortDescriptor sortDescriptorWithKey:@"major" ascending:YES];
        NSSortDescriptor * minorDesc = [NSSortDescriptor sortDescriptorWithKey:@"minor" ascending:YES];

        self.beacons = [beacons sortedArrayUsingDescriptors:@[uuidDesc, majorDesc, minorDesc]];
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}

#pragma mark - UITableViewDatasource 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.beacons.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 96.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * beaconIdentifier = @"beaconIdentifier";
    
    BeaconCell *cell = [tableView dequeueReusableCellWithIdentifier:beaconIdentifier];
    if (!cell) {
        cell = [[BeaconCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:beaconIdentifier];
    }
    
    BMBeacon * beacon = [self beaconForIndexPath:indexPath];
    [cell setMinor:[[beacon minor] stringValue]];
    [cell setMajor:[[beacon major] stringValue]];
    [cell setIdentifier:[beacon uuid]];
    
    ESTBeacon * realBeacon = [self searchForRealBeacon:beacon];
    if (realBeacon) {
        [cell setProximity:[NSString stringWithFormat:@"%.2f m", [[realBeacon distance] floatValue]]];
    } else {
        [cell setProximity:@"-"];
    }
    
    return cell;
}

- (BMBeacon*)beaconForIndexPath:(NSIndexPath*)indexPath {
    if (indexPath.row < self.beacons.count) {
        return [self.beacons objectAtIndex:indexPath.row];
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        BMBeacon * beacon = [self beaconForIndexPath:indexPath];
        
        [BMBeaconsClient deleteBeaconUID:beacon.uid success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            [self loadData];
            [tableView setEditing:NO animated:YES];
            
        } failtrue:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [tableView setEditing:NO animated:YES];
            
        }];
        
    }
}

#pragma mark - Actions

- (void)addNewBeacons:(id)sender {
    NewBeaconsViewController * newBeaconsViewController = [[NewBeaconsViewController alloc] initWithStyle:UITableViewStylePlain];
    [newBeaconsViewController setAlreadyAddedBeacons:self.beacons];
    [self.navigationController pushViewController:newBeaconsViewController animated:YES];
}

- (void)saveDistance {
    [BMFileManager showFile];
    
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"major == %@ AND minor == %@ AND distance >= 0", @1, @2];
    
    NSArray * filteredBeacons = [self.realBeacons filteredArrayUsingPredicate:predicate];
    
    if ([filteredBeacons count] == 1) {
        
        ESTBeacon * beacon = [filteredBeacons objectAtIndex:0];
        [BMFileManager writeAtEndOfFile:[NSString stringWithFormat:@"%f", floorf([beacon.distance floatValue] * 100)]];
        [BMFileManager showFile];
        
    } else {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"BEACON NOT FOUND"
                                                             message:nil
                                                            delegate:nil
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:nil];
        [alertView show];
    }
    
    return;
}

#pragma mark - Additions 

- (ESTBeacon*)searchForRealBeacon:(BMBeacon*)beacon {
    
    NSUUID * uuid = [[NSUUID alloc] initWithUUIDString:beacon.uuid];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"proximityUUID == %@ AND major == %@ AND minor == %@", uuid, beacon.major, beacon.minor];
    
    NSArray * filteredBeacons = [self.realBeacons filteredArrayUsingPredicate:predicate];
    
    if (filteredBeacons.count > 0) {
        return [filteredBeacons objectAtIndex:0];
    }
    return nil;
}

#pragma mark - Beacon manager delegate

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region {
    self.realBeacons = beacons;
    [self.tableView reloadData];
}


@end

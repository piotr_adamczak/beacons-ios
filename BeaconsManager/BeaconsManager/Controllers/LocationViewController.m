//
//  LocationViewController.m
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "LocationViewController.h"

#import "BMSetupManager.h"

//Views
#import "BMActionView.h"
#import "BMImageActionView.h"
#import "BMVideoActionView.h"
#import "BMAudioActionView.h"

//Models
#import "BMBeaconPlacement.h"

//Helpers
#import "MathHelper.h"

@interface LocationViewController ()
@property (nonatomic, strong) NSArray * maps;
@property (nonatomic, strong) BMActionView * currentActionView;

@property (nonatomic, strong) NSArray * realBeacons;
@property (nonatomic, strong) NSArray * apiBeacons;
@property (nonatomic, strong) BMMap * currentMap;
@property (nonatomic, strong) ESTBeaconManager* beaconManager;
@property (nonatomic, strong) UIButton * debugButton;
@property (nonatomic, strong) NSString * oldTitle;
@property (nonatomic, assign) BOOL debugMap;
@property (nonatomic, assign) BOOL actionOpened;
@property (nonatomic, strong) NSDate * lastActionTime;
@end

@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:NSLocalizedString(@"Location", nil)];
    [self loadMaps];
    
    self.beaconManager = [BMBeaconManager standardBeaconManagerWithDelegate:self];
    
    [BMBeaconManager fetchBeaconsWithBlock:^(BOOL success, NSArray *beacons) {
        self.apiBeacons = beacons;
    }];
    
    [self.mapView setShowRealRadius:YES];
    [self.mapView updateCurrentPosition:self.mapView.center];
    
    self.lastActionTime = [NSDate date];
    
    [self setupNavigationBarButtons];
}

- (void)setupNavigationBarButtons {
    self.debugButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.debugButton addTarget:self action:@selector(debugButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.debugButton setImage:[UIImage imageNamed:@"debug_on"] forState:UIControlStateNormal];
    [self.debugButton setFrame:CGRectMake(0, 0, [self.debugButton imageForState:UIControlStateNormal].size.width, [self.debugButton imageForState:UIControlStateNormal].size.height)];

    UIBarButtonItem * debugButton = [[UIBarButtonItem alloc] initWithCustomView:self.debugButton];
    [self.navigationItem setLeftBarButtonItem:debugButton];

    UIBarButtonItem * refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadData)];
    [self.navigationItem setRightBarButtonItem:refreshButton];
}

- (void)loadMaps {
    [BMSetupManager fetchSetupWithBlock:^(BOOL success, NSArray *places, NSArray *maps) {
        self.maps = maps;
        
        if (maps.count > 0) {
            self.currentMap = [maps firstObject];
            [self setMap:self.currentMap];
            
            if (self.currentMap.mapType == BMMapTypeBeaconsActions) {
                [self.mapView setShowRadiusOnlyWhenCloserThan2Meters:YES];
                [self.mapView.currentPositionView setAlpha:0.0];
            } else {
                [self.mapView setShowRadiusOnlyWhenCloserThan2Meters:NO];
                [self.mapView.currentPositionView setAlpha:0.7];
            }
            
            [self loadData];
        }
    }];
}

- (void)performActionForObject:(BMMapAction *)object {
    
    if (self.actionOpened) return;
    
    NSLog(@"%f", [self.lastActionTime timeIntervalSinceNow]);
    
    if ([self.lastActionTime timeIntervalSinceNow] > -2) return;
    
    BMActionView * actionView;
    
    Class actionClass;
    
    switch ([object BMMapActionType]) {
        case BMMapActionTypeText:
            actionClass = [BMActionView class];
            break;
        case BMMapActionTypeImage:
            actionClass = [BMImageActionView class];
            break;
        case BMMapActionTypeVideo:
            actionClass = [BMVideoActionView class];
            break;
        case BMMapActionTypeAudio:
            actionClass = [BMAudioActionView class];
            break;
        default:
            break;
    }
    
    actionView = [[actionClass alloc] initWithFrame:CGRectZero];
    [actionView setText:object.content];
    [actionView setMediaUrl:[object absouluteMediaURL]];

    self.currentActionView = actionView;
    
    [self.view addSubview:actionView];
    [actionView setFrame:self.view.bounds];
    [actionView.closeButton addTarget:self action:@selector(closeInfoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [actionView setVisible:YES completion:^(BOOL finished) {
    }];
    
    self.oldTitle = self.title;
    [self setTitle:object.name];
    
    UIBarButtonItem * closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(closeInfoButtonAction:)];
    [self.navigationItem setLeftBarButtonItem:closeButton];
    [self.navigationItem setRightBarButtonItem:nil];
    self.actionOpened = YES;
    
}

- (void)stopAllActions {
    [self closeInfoButtonAction:nil];
}

- (void)closeInfoButtonAction:(id)sender {
    
    self.actionOpened = NO;
    self.lastActionTime = [NSDate date];
    
    __weak LocationViewController * weakSelf = self;
    
    [self.currentActionView stopMedia];
    [self setupNavigationBarButtons];
    [self setTitle:self.oldTitle];
    
    [self.currentActionView setVisible:NO completion:^(BOOL finished) {
        [weakSelf.currentActionView removeFromSuperview];
    }];
}

#pragma mark - Additions

- (BMBeaconPlacement*)beaconPlacementForEstimoteBeacon:(ESTBeacon*)beacon {
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"beacon.uuid == %@ AND beacon.major == %@ AND beacon.minor == %@", [beacon.proximityUUID UUIDString], beacon.major, beacon.minor];
    
    NSArray * filteredBeacons = [self.beaconPlacements filteredArrayUsingPredicate:predicate];
    
    if (filteredBeacons.count > 0) {
        return [filteredBeacons objectAtIndex:0];
    }
    return nil;
}

#pragma mark - Beacon Manager Delegate

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region {
    
    NSPredicate * predicateForMinus = [NSPredicate predicateWithFormat:@"distance > 0"];
    NSSortDescriptor * descriptior = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    self.realBeacons = [[beacons filteredArrayUsingPredicate:predicateForMinus] sortedArrayUsingDescriptors:@[descriptior]];

    for (ESTBeacon * estimoteBeacon in self.realBeacons) {
        BMBeaconPlacement * beaconPlacement = [self beaconPlacementForEstimoteBeacon:estimoteBeacon];
        
        if (beaconPlacement && [estimoteBeacon.distance floatValue] > 0) {
            beaconPlacement.realRadius = estimoteBeacon.distance;
            [self checkActionForBeaconPlacement:beaconPlacement];
            NSUInteger index = [self.beaconPlacements indexOfObject:beaconPlacement];
            [self.beaconPlacements replaceObjectAtIndex:index withObject:beaconPlacement];
        }
    }
    
    [self.mapView setBeaconPlacements:self.beaconPlacements];
    
    
    if (self.currentMap.mapType == BMMapTypeLocationActions) {
        CGPoint currentPosition = [self calculateLocation];
        
        if (!CGPointEqualToPoint(currentPosition, CGPointZero)) {
            [self.mapView updateCurrentPosition:currentPosition];
        }
    }
}

- (CGPoint)calculateLocation {
    
    if ([self.realBeacons count] < 3) return CGPointZero;
    
    ESTBeacon * beacon0 = [self.realBeacons objectAtIndex:0];
    ESTBeacon * beacon1 = [self.realBeacons objectAtIndex:1];
    ESTBeacon * beacon2 = [self.realBeacons objectAtIndex:2];
                        
    BMBeaconPlacement * beaconPlacement0 = [self beaconPlacementForEstimoteBeacon:beacon0];
    BMBeaconPlacement * beaconPlacement1 = [self beaconPlacementForEstimoteBeacon:beacon1];
    BMBeaconPlacement * beaconPlacement2 = [self beaconPlacementForEstimoteBeacon:beacon2];

    if (!beaconPlacement0 || !beaconPlacement1 || !beaconPlacement2) return CGPointZero;

    CGPoint A = [beaconPlacement0 realCenterInMeters];
    CGPoint B = [beaconPlacement1 realCenterInMeters];
    CGPoint C = [beaconPlacement2 realCenterInMeters];
    CGPoint D; // out location
    
    CGFloat realWidth = [self.currentMap.width floatValue];
    CGFloat realHeight= [self.currentMap.height floatValue];
    
    CGFloat maxLength = MAX(realWidth, realHeight);
    CGFloat ADLengthInMeters = MIN([[beacon0 distance] floatValue], maxLength);
    CGFloat BDLengthInMeters = MIN([[beacon1 distance] floatValue], maxLength);
    CGFloat CDLengthInMeters = MIN([[beacon2 distance] floatValue], maxLength);
    
    if (ADLengthInMeters < 0) return CGPointZero;
    if (BDLengthInMeters < 0) return CGPointZero;
    if (CDLengthInMeters < 0) return CGPointZero;
    
    
    D = [self getCoordinateWithBeaconA:A beaconB:B beaconC:C distanceA:ADLengthInMeters distanceB:BDLengthInMeters distanceC:CDLengthInMeters];
    
    D.x = MIN(MAX(0,D.x),realWidth);
    D.y = MIN(MAX(0,D.y),realHeight);
    
    if (ADLengthInMeters < 2.3) {
        [self.mapView updateHelperPosition:[self convertToImagePointFromMetersPoint:D]];
        //Get nearest beacon and apply path
        D = [self applyLocationPathWithBeaconPlacement:beaconPlacement0 beacon:beacon0 point:D];
        D.x = MIN(MAX(0,D.x),realWidth);
        D.y = MIN(MAX(0,D.y),realHeight);
        
    } else {
        [self.mapView hideHelper];
    }
    
    D = [self convertToImagePointFromMetersPoint:D];
    
    return D;
}

- (CGPoint)getCoordinateWithBeaconA:(CGPoint)a beaconB:(CGPoint)b beaconC:(CGPoint)c distanceA:(CGFloat)dA distanceB:(CGFloat)dB distanceC:(CGFloat)dC {
    CGFloat W, Z, x, y, y2;
    
    W = dA*dA - dB*dB - a.x*a.x - a.y*a.y + b.x*b.x + b.y*b.y;
    Z = dB*dB - dC*dC - b.x*b.x - b.y*b.y + c.x*c.x + c.y*c.y;
    
    x = (W*(c.y-b.y) - Z*(b.y-a.y)) / (2 * ((b.x-a.x)*(c.y-b.y) - (c.x-b.x)*(b.y-a.y)));
    y = (W - 2*x*(b.x-a.x)) / (2*(b.y-a.y));
    //y2 is a second measure of y to mitigate errors
    y2 = (Z - 2*x*(c.x-b.x)) / (2*(c.y-b.y));
    
    y = (y + y2) / 2;
    return CGPointMake(x, y);
}

- (CGPoint)convertToImagePointFromMetersPoint:(CGPoint)meterPoint {
    
    CGFloat centimetersX = meterPoint.x * 100;
    CGFloat centimetersY = meterPoint.y * 100;
    
    CGSize realSize = [self.currentMap realSize];
    CGSize imageSize = [self.currentMap imageSize];
    
    CGFloat multiplierX = centimetersX / realSize.width;
    CGFloat multiplierY = centimetersY / realSize.height;
    
    CGPoint imagePoint = CGPointMake(multiplierX * imageSize.width, multiplierY * imageSize.height);
    return imagePoint;
}

- (CGPoint)applyLocationPathWithBeaconPlacement:(BMBeaconPlacement*)beaconPlacement beacon:(ESTBeacon*)beacon point:(CGPoint)point {
    
    CGFloat realWidth = [self.currentMap.width floatValue];
    CGFloat realHeight= [self.currentMap.height floatValue];
    CGFloat maxLength = MAX(realWidth, realHeight);
    
    CGPoint A = [beaconPlacement realCenterInMeters];
    CGFloat R = MIN([[beacon distance] floatValue], maxLength);
    
    CGPoint relativePoint = [MathHelper substractPoint:point withPoint:A];
    CGFloat multipler = R / [MathHelper lengthBetweenPoints:point withPoint:A];
    CGPoint VP = [MathHelper multiplyPoint:relativePoint withValue:multipler];
    
    CGPoint solution1 = [MathHelper addPoint:VP toPoint:A];
    CGPoint solution2 = [MathHelper substractPoint:A withPoint:VP];
    
    CGFloat lengthBetweenPointAndSolution1 = [MathHelper lengthBetweenPoints:point withPoint:solution1];
    CGFloat lengthBetweenPointAndSolution2 = [MathHelper lengthBetweenPoints:point withPoint:solution2];
    CGFloat lengthBetweenPointAndBeacon = [MathHelper lengthBetweenPoints:point withPoint:A];
    CGFloat lengthBetweenBeaconAndSolution1 = [MathHelper lengthBetweenPoints:A withPoint:solution1];
    CGFloat lengthBetweenBeaconAndSolution2 = [MathHelper lengthBetweenPoints:A withPoint:solution2];

    if (lengthBetweenPointAndBeacon < MIN(lengthBetweenBeaconAndSolution1, lengthBetweenBeaconAndSolution2)) {
        return point;
    } else {
        if (lengthBetweenPointAndSolution1 < lengthBetweenPointAndSolution2) {
            return solution1;
        } else {
            return solution2;
        }
    }
}

#pragma mark - Beacon actions 

- (void)checkActionForBeaconPlacement:(BMBeaconPlacement*)beaconPlacement {
    
    BMBeacon * beacon = beaconPlacement.beacon;
    
    for (BMMapAction * action in self.actions) {
        
        if (action.beaconId && [action.beaconId isEqualToNumber:beacon.uid]) {
            
            if (([beaconPlacement.realRadius floatValue] * 100) <= [action.proximity floatValue]) {
                [self performActionForObject:action];
                return;
            }
        }
    }
}

#pragma mark - Debugging

- (void)debugButtonAction:(id)sender {
    
    _debugMap = !self.debugMap;
    
    [self.mapView setDebugEnabled:_debugMap];
    
    UIImage * image = _debugMap ? [UIImage imageNamed:@"debug_on"] : [UIImage imageNamed:@"debug_off"];
    [self.debugButton setImage:image forState:UIControlStateNormal];
}

@end

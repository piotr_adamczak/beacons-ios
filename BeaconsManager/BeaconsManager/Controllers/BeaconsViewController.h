//
//  BeaconsViewController.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <EstimoteSDK/ESTBeaconManager.h>

@interface BeaconsViewController : UITableViewController<ESTBeaconManagerDelegate>

- (IBAction)addNewBeacons:(id)sender;

@end

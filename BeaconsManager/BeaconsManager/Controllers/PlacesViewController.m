//
//  PlacesViewController.m
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "PlacesViewController.h"

#import "MapsViewController.h"

//Models
#import "BMPlace.h"
#import "BMMap.h"

//Manager
#import "BMSetupManager.h"

@interface PlacesViewController ()
@property (nonatomic, strong) NSArray * places;
@property (nonatomic, strong) NSArray * maps;
@end

@implementation PlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:NSLocalizedString(@"Places", nil)];
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];

    
    UIBarButtonItem * refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadData)];
    [self.navigationItem setRightBarButtonItem:refreshButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)loadData {
    [BMSetupManager fetchSetupWithBlock:^(BOOL success, NSArray *places, NSArray *maps) {
        self.places = places;
        self.maps = maps;
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.places.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * beaconIdentifier = @"beaconIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:beaconIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:beaconIdentifier];
    }
    
    BMPlace * place = [self placeForIndexPath:indexPath];
    [cell.textLabel setText:[place name]];
    [cell.textLabel setAdjustsFontSizeToFitWidth:YES];
    
    return cell;
}

- (BMPlace*)placeForIndexPath:(NSIndexPath*)indexPath {
    if (indexPath.row < self.places.count) {
        return [self.places objectAtIndex:indexPath.row];
    }
    return nil;
}

#pragma mark - UITableViewDelegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BMPlace * place = [self placeForIndexPath:indexPath];

    NSArray * maps = [self.maps filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeId == %@",place.uid]];
    
    MapsViewController * mapsViewController = [[MapsViewController alloc] initWithStyle:UITableViewStylePlain];
    [mapsViewController setMaps:maps];
    [mapsViewController setPlaceName:place.name];
    
    [self.navigationController pushViewController:mapsViewController animated:YES];
}

@end

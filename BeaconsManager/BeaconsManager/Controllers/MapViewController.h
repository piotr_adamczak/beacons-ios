//
//  MapViewController.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <UIKit/UIKit.h>

//Model
#import "BMMap.h"
#import "BMMapAction.h"

//Views
#import "BMMapView.h"

@interface MapViewController : UIViewController

@property (nonatomic, strong) BMMapView * mapView;
@property (nonatomic, strong) NSMutableArray * beaconPlacements;
@property (nonatomic, strong) NSArray * actions;

- (void)setMap:(BMMap *)map;
- (void)loadData;
- (void)mapActionTapped:(UIImageView*)sender;
- (void)performActionForObject:(BMMapAction*)object;

@end

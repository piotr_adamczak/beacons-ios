//
//  MapsViewController.m
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "MapsViewController.h"
#import "MapViewController.h"

//Models
#import "BMMap.h"

@interface MapsViewController ()
@property (nonatomic, strong) NSArray * maps;
@end

@implementation MapsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:self.placeName];

}

- (void)setMaps:(NSArray *)maps {
    _maps = maps;
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.maps.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * beaconIdentifier = @"beaconIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:beaconIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:beaconIdentifier];
    }
    
    BMMap * map = [self mapForIndexPath:indexPath];
    [cell.textLabel setText:[map name]];
    [cell.textLabel setAdjustsFontSizeToFitWidth:YES];
    
    return cell;
}

- (BMMap*)mapForIndexPath:(NSIndexPath*)indexPath {
    if (indexPath.row < self.maps.count) {
        return [self.maps objectAtIndex:indexPath.row];
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BMMap * map = [self mapForIndexPath:indexPath];
    
    MapViewController * mapViewController = [[MapViewController alloc] init];
    [mapViewController setMap:map];
    [self.navigationController pushViewController:mapViewController animated:YES];
}

@end

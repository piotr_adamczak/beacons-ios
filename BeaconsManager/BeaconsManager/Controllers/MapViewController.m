//
//  MapViewController.m
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "MapViewController.h"

//Models
#import "BMMap.h"
#import "BMBeaconPlacement.h"
#import "BMMapAction.h"

//Managers
#import "BMActionsManager.h"


@interface MapViewController ()
@property (nonatomic, strong) BMMap * map;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView = [[BMMapView alloc] initWithFrame:self.view.bounds];
    [self.mapView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.mapView setShowRealRadius:NO];
    [self.view addSubview:self.mapView];
    
    [self setMap:_map];
    [self loadData];
    
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    
    UIBarButtonItem * refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadData)];
    [self.navigationItem setRightBarButtonItem:refreshButton];
}

- (void)loadData {
    
    [BMActionsManager fetchActionsForMap:self.map block:^(BOOL success, NSArray *places, NSArray *maps) {
        self.beaconPlacements = [NSMutableArray arrayWithArray:places];
        self.actions = [NSMutableArray arrayWithArray:maps];
        [self.mapView setMapAcitons:maps target:self];
        [self.mapView setBeaconPlacements:places];
    }];
}

- (void)setMap:(BMMap *)map {
    _map = map;
    [self.mapView setImageURL:[map mediaURL]];
    [self setTitle:map.name];
}

- (void)mapActionTapped:(UITapGestureRecognizer *)sender {

    NSPredicate * actionPredicate = [NSPredicate predicateWithFormat:@"uid == %d", sender.view.tag];
    NSArray * filteredActions = [self.actions filteredArrayUsingPredicate:actionPredicate];
    
    if (filteredActions.count == 1) {
        BMMapAction * mapAction = [filteredActions firstObject];
        [self performActionForObject:mapAction];
    }
}

- (void)performActionForObject:(BMMapAction *)object {
    NSLog(@"%@", object.name);
}

@end

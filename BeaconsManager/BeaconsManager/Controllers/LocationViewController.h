//
//  LocationViewController.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"
#import "BMBeaconManager.h"

@interface LocationViewController : MapViewController<ESTBeaconManagerDelegate>

- (void)stopAllActions;

@end

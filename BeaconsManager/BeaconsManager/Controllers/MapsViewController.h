//
//  MapsViewController.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapsViewController : UITableViewController

@property (nonatomic, strong) NSString * placeName;

- (void)setMaps:(NSArray *)maps;

@end

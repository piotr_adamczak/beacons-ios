//
//  NewBeaconsViewController.h
//  BeaconsManager
//
//  Created by Piotrek on 16.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <UIKit/UIKit.h>

//Beacons
#import <EstimoteSDK/ESTBeacon.h>
#import <EstimoteSDK/ESTBeaconManager.h>
#import <EstimoteSDK/ESTBeaconRegion.h>

@interface NewBeaconsViewController : UITableViewController<ESTBeaconManagerDelegate>

@property (nonatomic, strong) NSArray * alreadyAddedBeacons;

@end

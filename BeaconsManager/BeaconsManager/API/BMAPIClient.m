//
//  BMAPIClient.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMAPIClient.h"

#import "AFJSONRequestOperation.h"

// Constants
NSString * const kBMAuthenticationToken = @"BMAuthenticationToken";

//NSString * const kBMBaseURLString = @"http://192.168.0.132:3000";
NSString * const kBMBaseURLString = @"http://localhost:3000";

@implementation BMAPIClient

+ (BMAPIClient *)sharedClient {
    static BMAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[BMAPIClient alloc] initWithBaseURL:[NSURL URLWithString:[kBMBaseURLString stringByAppendingString:@"/api"]]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    // Register HTTP operation class
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    // Accept HTTP Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
    [self setDefaultHeader:@"Accept" value:@"application/json"];
    [self setParameterEncoding:AFJSONParameterEncoding];

    if ([self hasValidAuthToken]) {
        [self setAuthorizationHeaderWithToken:self.authToken];
    }
    
    return self;
}

- (void)handleNetworkingError:(NSError *)error forOperation:(AFHTTPRequestOperation *)operation {
    if ([error code] != NSURLErrorCancelled) {
        // Something went wrong
        //[[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
    }
}

- (NSString *)authToken {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kBMAuthenticationToken];
}

- (void)setAuthToken:(NSString *)authToken {
    [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:kBMAuthenticationToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)hasValidAuthToken {
    return self.authToken.length > 0;
}

@end
//
//  BMProfileClient.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMBeaconsClient.h"
#import "BMAPIClient.h"

@implementation BMBeaconsClient

+ (void)fetchBeaconsWithSuccess:(BMJsonRequestOperationSuccessHandler)success failtrue:(BMJsonRequestOperationFailureHandler)failtrue {
    [BMBeaconsClient JSONRequestOperationWithAddress:@"beacons" method:RequestMethodGET parameters:nil success:success failure:failtrue];
}

+ (void)postBeaconDictionary:(NSDictionary *)beaconDict success:(BMJsonRequestOperationSuccessHandler)success failtrue:(BMJsonRequestOperationFailureHandler)failtrue {
    [BMBeaconsClient JSONRequestOperationWithAddress:[NSString stringWithFormat:@"%@/beacons", kBMBaseURLString] method:RequestMethodPOST parameters:beaconDict success:success failure:failtrue];
}

+ (void)deleteBeaconUID:(NSNumber *)uid success:(BMJsonRequestOperationSuccessHandler)success failtrue:(BMJsonRequestOperationFailureHandler)failtrue {
    [BMBeaconsClient JSONRequestOperationWithAddress:[NSString stringWithFormat:@"%@/beacons/%@", kBMBaseURLString, uid] method:RequestMethodDELETE parameters:nil success:success failure:failtrue];
    
}

@end

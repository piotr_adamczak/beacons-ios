//
//  BMJSONRequestOperation.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMJSONRequestOperation.h"

//Client
#import "BMAPIClient.h"

@implementation BMJSONRequestOperation

+ (NSString *)requestMethodNameFromType:(RequestMethod)method {
    switch (method) {
        case RequestMethodGET:
            return @"GET";
            break;
            
        case RequestMethodPOST:
            return @"POST";
            break;
            
        case RequestMethodPUT:
            return @"PUT";
            break;
            
        case RequestMethodDELETE:
            return @"DELETE";
            break;
            
        case RequestMethodPATCH:
            return @"PATCH";
            break;
            
        case RequestMethodHEAD:
            return @"HEAD";
            break;
            
        default:
            break;
    }
    
    return nil;
}

+ (NSMutableURLRequest*)requestWithAddress:(NSString*)address method:(RequestMethod)method parameters:(NSDictionary *)parameters {
    
    NSString *firstLetter = [address substringToIndex:1];
    if ([firstLetter isEqualToString:@"/"]) {
        address = [address substringFromIndex:1];
    }
    
    BMAPIClient * client = [BMAPIClient sharedClient];
    NSString * stringMethod = [BMJSONRequestOperation requestMethodNameFromType:method];
    return [client requestWithMethod:stringMethod path:address parameters:parameters];
}

+ (BMJSONRequestOperation*)JSONRequestOperationWithAddress:(NSString *)address
                                                    method:(RequestMethod)method
                                                parameters:(NSDictionary *)parameters
                                                   success:(BMJsonRequestOperationSuccessHandler)success
                                                   failure:(BMJsonRequestOperationFailureHandler)failure {
    
    NSURLRequest * urlRequest = [BMJSONRequestOperation requestWithAddress:address method:method parameters:parameters];
    
    BMJSONRequestOperation * operation = [BMJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:success failure:failure];
    [[[BMAPIClient sharedClient] operationQueue] addOperation:operation];
    return operation;
}

+ (NSDateFormatter *)UTCDate {
    static NSDateFormatter* __formatter;
    
    if (__formatter == nil) {
        __formatter = [[NSDateFormatter alloc] init];
        [__formatter setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [__formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [__formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    }
    return __formatter;
}

@end

//
//  BMMapClient.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMMapClient.h"
#import "BMAPIClient.h"

@implementation BMMapClient

+ (void)fetchMapDetailsForMapId:(NSNumber*)mapId
                        success:(BMJsonRequestOperationSuccessHandler)success
                       failtrue:(BMJsonRequestOperationFailureHandler)failtrue {
    
    NSString *addres = [@"objects_for_map/" stringByAppendingString:[mapId stringValue]];
    [BMMapClient JSONRequestOperationWithAddress:addres method:RequestMethodGET parameters:nil success:success failure:failtrue];
}



@end

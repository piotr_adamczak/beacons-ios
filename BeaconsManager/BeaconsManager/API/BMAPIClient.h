//
//  BMAPIClient.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "AFHTTPClient.h"

extern NSString * const kBMAuthenticationToken;
extern NSString * const kBMBaseURLString;

@interface BMAPIClient : AFHTTPClient

+ (BMAPIClient *)sharedClient;

@property (nonatomic, strong) NSString *authToken;

- (void)handleNetworkingError:(NSError *)error forOperation:(AFHTTPRequestOperation *)operation;
- (BOOL)hasValidAuthToken;

@end
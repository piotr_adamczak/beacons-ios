//
//  BMSetupClient.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMSetupClient.h"
#import "BMAPIClient.h"

@implementation BMSetupClient

+ (void)fetchSetupWithSuccess:(BMJsonRequestOperationSuccessHandler)success
                     failtrue:(BMJsonRequestOperationFailureHandler)failtrue {
    [BMSetupClient JSONRequestOperationWithAddress:@"setup" method:RequestMethodGET parameters:nil success:success failure:failtrue];
}

@end

//
//  BMSetupClient.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMJSONRequestOperation.h"

@interface BMSetupClient : BMJSONRequestOperation

+ (void)fetchSetupWithSuccess:(BMJsonRequestOperationSuccessHandler)success
                     failtrue:(BMJsonRequestOperationFailureHandler)failtrue;

@end

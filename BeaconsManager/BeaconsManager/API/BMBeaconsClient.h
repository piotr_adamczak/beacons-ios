//
//  BMProfileClient.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMJSONRequestOperation.h"

@interface BMBeaconsClient : BMJSONRequestOperation

+ (void)fetchBeaconsWithSuccess:(BMJsonRequestOperationSuccessHandler)success
                       failtrue:(BMJsonRequestOperationFailureHandler)failtrue;

+ (void)postBeaconDictionary:(NSDictionary*)beaconDict
                     success:(BMJsonRequestOperationSuccessHandler)success
                    failtrue:(BMJsonRequestOperationFailureHandler)failtrue;

+ (void)deleteBeaconUID:(NSNumber*)uid
                success:(BMJsonRequestOperationSuccessHandler)success
               failtrue:(BMJsonRequestOperationFailureHandler)failtrue;
@end

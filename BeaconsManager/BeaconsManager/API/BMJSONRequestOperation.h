//
//  BMJSONRequestOperation.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "AFJSONRequestOperation.h"

typedef enum RequestMethod {
    RequestMethodInvalid = -1,
    RequestMethodGET,
    RequestMethodPOST,
    RequestMethodPUT,
    RequestMethodDELETE,
    RequestMethodPATCH,
    RequestMethodHEAD
} RequestMethod;

typedef void (^BMJsonRequestOperationSuccessHandler)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON);
typedef void (^BMJsonRequestOperationFailureHandler)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON);

@interface BMJSONRequestOperation : AFJSONRequestOperation

+ (NSMutableURLRequest*)requestWithAddress:(NSString*)address method:(RequestMethod)method parameters:(NSDictionary*)parameters;
+ (BMJSONRequestOperation*)JSONRequestOperationWithAddress:(NSString *)address
                                                    method:(RequestMethod)method
                                                parameters:(NSDictionary *)parameters
                                                   success:(BMJsonRequestOperationSuccessHandler)success
                                                   failure:(BMJsonRequestOperationFailureHandler)failure;

+ (NSDateFormatter *)UTCDate;
@end

//
//  BMBeaconManager.m
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMBeaconManager.h"
#import <Mantle/MTLJSONAdapter.h>

//Api Client
#import "BMBeaconsClient.h"

//Models
#import "BMBeacon.h"

@interface BMBeaconManager()
@property (nonatomic, strong) NSArray * beacons;
@end

@implementation BMBeaconManager

AUSINGLETON_IMPL_FOR_CLASS(BMBeaconManager);

+ (void)fetchBeaconsWithBlock:(void (^)(BOOL, NSArray *))block {
   
    [BMBeaconsClient fetchBeaconsWithSuccess:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSMutableArray * beacons = [[NSMutableArray alloc] init];
        
        for (NSDictionary *beacon in JSON[@"beacons"]) {
            if ([beacon isKindOfClass:[NSDictionary class]]) {
                BMBeacon *beaconObject = [MTLJSONAdapter modelOfClass:[BMBeacon class] fromJSONDictionary:beacon error:nil];
                [beacons addObject:beaconObject];
            }
        }
        
        [[BMBeaconManager sharedBMBeaconManager] setBeacons:beacons];
        
        if (block) {
            block(YES, beacons);
        }
        
    } failtrue:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        if (block) {
            block(NO, nil);
        }
        
    }];
}

- (void)fetchBeacons:(void (^)(NSArray *))block {
    
    if (self.beacons) {
        block(self.beacons);
    } else {
        
        [BMBeaconManager fetchBeaconsWithBlock:^(BOOL success, NSArray *beacons) {
            block(beacons);
        }];
    }
}

- (void)postBeacon:(ESTBeacon*)beacon block:(void (^)(BOOL))block {
    
    BMBeacon * bmBeacon = [[BMBeacon alloc] init];
    bmBeacon.uuid = [beacon.proximityUUID UUIDString];
    bmBeacon.major = beacon.major;
    bmBeacon.minor = beacon.minor;
    
    NSDictionary * dictionary = [MTLJSONAdapter JSONDictionaryFromModel:bmBeacon];
    [BMBeaconsClient postBeaconDictionary:dictionary success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        block(YES);
    } failtrue:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        block(NO);
    }];
}

+ (ESTBeaconManager *)standardBeaconManagerWithDelegate:(id<ESTBeaconManagerDelegate>)delegate {
    
    ESTBeaconManager * beaconManager = [[ESTBeaconManager alloc] init];
    beaconManager.delegate = delegate;
    
    NSUUID * uuid = [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"];
    
    ESTBeaconRegion* region = [[ESTBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"EstimoteSampleRegion"];
    [beaconManager startRangingBeaconsInRegion:region];
    
    return beaconManager;
}

@end

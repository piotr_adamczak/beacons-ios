//
//  BMSetupManager.m
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMSetupManager.h"
#import <Mantle/MTLJSONAdapter.h>

//Api Client
#import "BMSetupClient.h"

//Models
#import "BMMap.h"
#import "BMPlace.h"

@interface BMSetupManager()
@property (nonatomic, strong) NSArray * places;
@property (nonatomic, strong) NSArray * maps;
@end

@implementation BMSetupManager

AUSINGLETON_IMPL_FOR_CLASS(BMSetupManager);

+ (void)fetchSetupWithBlock:(void (^)(BOOL, NSArray *, NSArray *))block {
    
    [BMSetupClient fetchSetupWithSuccess:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSMutableArray * places = [[NSMutableArray alloc] init];
        
        for (NSDictionary *place in JSON[@"places"]) {
            if ([place isKindOfClass:[NSDictionary class]]) {
                BMPlace *placeObject = [MTLJSONAdapter modelOfClass:[BMPlace class] fromJSONDictionary:place error:nil];
                [places addObject:placeObject];
            }
        }
        
        [[BMSetupManager sharedBMSetupManager] setPlaces:places];
        
        NSMutableArray * maps = [[NSMutableArray alloc] init];
        
        for (NSDictionary *map in JSON[@"maps"]) {
            if ([map isKindOfClass:[NSDictionary class]]) {
                BMPlace *mapObject = [MTLJSONAdapter modelOfClass:[BMMap class] fromJSONDictionary:map error:nil];
                [maps addObject:mapObject];
            }
        }
        
        [[BMSetupManager sharedBMSetupManager] setMaps:maps];
        
        if (block) {
            block(YES, places, maps);
        }
        
    } failtrue:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        if (block) {
            block(NO, nil, nil);
        }
        
    }];
}

- (void)fetchPlacesWithBlock:(void (^)(NSArray *))block {

    if (self.places) {
        block(self.places);
    } else {
        
        [BMSetupManager fetchSetupWithBlock:^(BOOL success, NSArray *places, NSArray *maps) {
            block(places);
        }];
    }
}

- (void)fetchMapsWithBlock:(void (^)(NSArray *))block {
    if (self.maps) {
        block(self.maps);
    } else {
        
        [BMSetupManager fetchSetupWithBlock:^(BOOL success, NSArray *places, NSArray *maps) {
            block(maps);
        }];
    }
}

@end

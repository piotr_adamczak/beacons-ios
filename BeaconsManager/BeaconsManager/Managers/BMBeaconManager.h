//
//  BMBeaconManager.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "MTLModel.h"

//Beacons
#import <EstimoteSDK/ESTBeacon.h>
#import <EstimoteSDK/ESTBeaconManager.h>
#import <EstimoteSDK/ESTBeaconRegion.h>


@interface BMBeaconManager : NSObject

AUSINGLETON_FOR_CLASS(BMBeaconManager);

+ (void)fetchBeaconsWithBlock:(void (^)(BOOL success, NSArray *beacons))block;
- (void)fetchBeacons:(void (^)(NSArray *beacons))block;
- (void)postBeacon:(ESTBeacon*)beacon block:(void (^)(BOOL success))block;
+ (ESTBeaconManager*)standardBeaconManagerWithDelegate:(id<ESTBeaconManagerDelegate>)delegate;

@end

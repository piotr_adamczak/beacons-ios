//
//  BMActionsManager.m
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMActionsManager.h"
#import <Mantle/MTLJSONAdapter.h>

//Api Client
#import "BMMapClient.h"

//Models
#import "BMMapAction.h"
#import "BMBeaconPlacement.h"

@implementation BMActionsManager

AUSINGLETON_IMPL_FOR_CLASS(BMActionsManager);

+ (void)fetchActionsForMap:(BMMap *)map block:(void (^)(BOOL, NSArray *, NSArray *))block {
    
    if (!map) return;
    
    [BMMapClient fetchMapDetailsForMapId:map.uid success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
  
        NSMutableArray * placements = [[NSMutableArray alloc] init];
        
        for (NSDictionary *place in JSON[@"beacons"]) {
            if ([place isKindOfClass:[NSDictionary class]]) {
                BMBeaconPlacement *placeObject = [MTLJSONAdapter modelOfClass:[BMBeaconPlacement class] fromJSONDictionary:place error:nil];
                [placements addObject:placeObject];
            }
        }
        
        NSMutableArray * actions = [[NSMutableArray alloc] init];
        
        for (NSDictionary *action in JSON[@"actions"]) {
            if ([action isKindOfClass:[NSDictionary class]]) {
                BMMapAction *actionObject = [MTLJSONAdapter modelOfClass:[BMMapAction class] fromJSONDictionary:action error:nil];
                [actionObject prepareForPlacements:placements];
                [actions addObject:actionObject];
            }
        }
        
        if (block) {
            block(YES, placements, actions);
        }
        
    } failtrue:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        if (block) {
            block(NO, nil, nil);
        }
        
    }];
}
@end

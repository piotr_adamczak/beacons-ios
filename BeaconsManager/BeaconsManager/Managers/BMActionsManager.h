//
//  BMActionsManager.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <Foundation/Foundation.h>

//Models
#import "BMMap.h"

@interface BMActionsManager : NSObject

AUSINGLETON_FOR_CLASS(BMActionsManager);

+ (void)fetchActionsForMap:(BMMap*)map block:(void (^)(BOOL success, NSArray *places, NSArray * maps))block;

@end

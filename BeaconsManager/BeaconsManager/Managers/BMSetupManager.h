//
//  BMSetupManager.h
//  BeaconsManager
//
//  Created by Piotrek on 15.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BMSetupManager : NSObject

AUSINGLETON_FOR_CLASS(BMSetupManager);

+ (void)fetchSetupWithBlock:(void (^)(BOOL success, NSArray *places, NSArray * maps))block;
- (void)fetchPlacesWithBlock:(void (^)(NSArray *places))block;
- (void)fetchMapsWithBlock:(void (^)(NSArray *maps))block;

@end

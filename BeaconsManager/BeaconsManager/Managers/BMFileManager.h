//
//  BMFileManager.h
//  BeaconsManager
//
//  Created by Piotrek on 24.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BMFileManager : NSObject

+ (void)clearFile;
+ (void)writeAtEndOfFile:(NSString *)content2;
+ (void)writeToFileValue:(NSString*)stringValue;
+ (void)showFile;

@end

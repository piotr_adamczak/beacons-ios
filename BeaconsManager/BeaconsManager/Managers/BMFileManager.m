//
//  BMFileManager.m
//  BeaconsManager
//
//  Created by Piotrek on 24.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BMFileManager.h"

@implementation BMFileManager

+ (void)clearFile {
    [BMFileManager writeToFileValue:@""];
}

+ (void)writeAtEndOfFile:(NSString *)content2
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/textfile.txt",
                          documentsDirectory];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:fileName])
    {
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:fileName];
        [fileHandle seekToEndOfFile];
        NSString *writedStr = [[NSString alloc]initWithContentsOfFile:fileName encoding:NSUTF8StringEncoding error:nil];
        content2 = [content2 stringByAppendingString:@"\n"];
        writedStr = [writedStr stringByAppendingString:content2];
        
        [writedStr writeToFile:fileName
                    atomically:NO
                      encoding:NSStringEncodingConversionAllowLossy
                         error:nil];
    }
    else {
        [BMFileManager writeToFileValue:content2];
    }
    
}

+ (void)writeToFileValue:(NSString*)stringValue{
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/textfile.txt",
                          documentsDirectory];
    //create content - four lines of text
    
    NSString *content = [stringValue stringByAppendingString:@"\n"];
    //save content to the documents directory
    [content writeToFile:fileName
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
    
}


//Method retrieves content from documents directory and
//displays it in an alert
+ (void)showFile {
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/textfile.txt",
                          documentsDirectory];
    NSString *content = [[NSString alloc] initWithContentsOfFile:fileName
                                                    usedEncoding:nil
                                                           error:nil];

    NSLog(@"\nFILE CONTENT:\n%@\nEND\n\n", content);
}

@end

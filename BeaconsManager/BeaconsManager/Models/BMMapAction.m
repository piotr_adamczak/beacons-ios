//
//  BMMapAction
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMMapAction.h"
#import "BMSetupManager.h"
#import "BMActionsManager.h"

@implementation BMMapAction
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self) {
        [[BMSetupManager sharedBMSetupManager] fetchMapsWithBlock:^(NSArray *maps) {
            
            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@", self.mapId];
            NSArray * filteredArray = [maps filteredArrayUsingPredicate:predicate];
            
            if (filteredArray.count > 0) {
                self.map = [filteredArray objectAtIndex:0];
                [self prepareForPlacements:self.placements];
            }
        }];
        
    }
    return self;
}

- (void)prepareForPlacements:(NSArray*)placements {
    
    _placements = placements;
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"beaconId == %@", self.beaconId];
    NSArray * filteredBeaconArray = [placements filteredArrayUsingPredicate:predicate];
    
    if (filteredBeaconArray.count > 0) {
        self.beaconPlacement = [filteredBeaconArray objectAtIndex:0];
        
        if (self.map.mapType == BMMapTypeBeaconsActions) {
            self.x = self.beaconPlacement.x;
            self.y = self.beaconPlacement.y;
        }
    }
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{
             @"uid": @"id",
             @"name": @"name",
             @"mapId": @"map_id",
             @"proximity": @"proximity",
             @"x" : @"x",
             @"y" : @"y",
             @"content" : @"content",
             @"beaconId" : @"beacon_id",
             @"actionType" : @"action_type",
             @"mediaUrl" : @"media_url",
             @"actionUrl" : @"action_url",
             @"createdAt" : @"created_at",
             @"updatedAt" : @"updated_at"
             };
}

- (BMMapActionType)BMMapActionType {
    return [self.actionType integerValue];
}


- (NSURL *)absouluteMediaURL {
    return [self absoluteURLOfPath:self.mediaUrl];
}

- (CGPoint)centerForImage {
    
    CGSize realSize = [self.map realSize];
    CGSize imageSize = [self.map imageSize];
    CGFloat proximity = [self.proximity floatValue];
    
    CGFloat relativeX = ([self.x floatValue] - proximity) / realSize.width;
    CGFloat relativeY = ([self.y floatValue] - proximity) / realSize.height;
    
    CGPoint realCenter = CGPointMake(relativeX * imageSize.width, relativeY * imageSize.height);
    return realCenter;
}

- (CGFloat)imageProximity {
    CGSize realSize = [self.map realSize];
    CGSize imageSize = [self.map imageSize];
    CGFloat proximity = [self.proximity floatValue];
    
    CGFloat relativeX = (proximity / realSize.width);
    return relativeX * imageSize.width;
}

@end

//
//  BMBeacon.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMBeacon.h"

@implementation BMBeacon

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{
             @"uid": @"id",
             @"uuid": @"uuid",
             @"major": @"major",
             @"minor": @"minor",
             @"colorId" : @"color",
             @"createdAt" : @"created_at",
             @"updatedAt" : @"updated_at"
             };
}

- (NSString *)minorAndMajor {
    return [NSString stringWithFormat:@"Minor: %@ Major: %@", self.minor, self.major];
}

- (NSString *)fullUUID {
   return [NSString stringWithFormat:@"%@ Minor: %@ Major: %@", self.uuid, self.minor, self.major];
}

@end

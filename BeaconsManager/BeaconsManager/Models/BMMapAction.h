//
//  BMMapAction.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMModelObject.h"

//Models
#import "BMMap.h"
#import "BMBeaconPlacement.h"
#import "BMBeacon.h"

typedef enum BMMapActionType : NSInteger {
    BMMapActionTypeText,
    BMMapActionTypeImage,
    BMMapActionTypeVideo,
    BMMapActionTypeAudio
} BMMapActionType;

@interface BMMapAction : BMModelObject

@property NSNumber *uid;
@property NSString *name;
@property NSNumber *x;
@property NSNumber *y;
@property NSNumber *proximity;
@property NSNumber *actionType;
@property NSString *content;
@property NSString *mediaUrl;
@property NSString *actionUrl;
@property NSNumber *mapId;
@property NSNumber *beaconId;
@property (nonatomic, strong) NSArray * placements;
@property (nonatomic, strong) BMMap * map;
@property (nonatomic, strong) BMBeaconPlacement * beaconPlacement;

- (CGPoint)centerForImage;
- (CGFloat)imageProximity;
- (BMMapActionType)BMMapActionType;
- (NSURL *)absouluteMediaURL;
- (void)prepareForPlacements:(NSArray*)placements;

@end

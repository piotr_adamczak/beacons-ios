//
//  BMMap.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMModelObject.h"

//Models
#import "BMPlace.h"

typedef enum BMMapType : NSInteger {
    BMMapTypeBeaconsActions,
    BMMapTypeLocationActions
} BMMapType;

@interface BMMap : BMModelObject

@property NSNumber *uid;
@property NSString *name;
@property NSString *url;
@property NSNumber *floor;
@property NSNumber *width;
@property NSNumber *height;
@property NSNumber *savedMapType;
@property NSNumber *imageWidth;
@property NSNumber *imageHeight;
@property NSNumber *placeId;
@property (nonatomic, strong) BMPlace * place;

- (NSURL*)mediaURL;
- (CGSize)imageSize;
- (CGSize)realSize;
- (BMMapType)mapType;

@end

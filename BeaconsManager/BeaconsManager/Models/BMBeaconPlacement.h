//
//  BMBeaconPlacement.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMModelObject.h"

//Models
#import "BMMap.h"
#import "BMBeacon.h"

@interface BMBeaconPlacement : BMModelObject

@property NSNumber *uid;
@property NSString *name;
@property NSNumber *x;
@property NSNumber *y;
@property NSNumber *realRadius;
@property NSNumber *beaconId;
@property NSNumber *mapId;
@property (nonatomic, strong) BMBeacon * beacon;
@property (nonatomic, strong) BMMap * map;

- (CGPoint)realCenterInMeters;
- (CGPoint)centerForImage;
- (NSNumber*)radius;
- (NSNumber*)radiusInPixels;

@end

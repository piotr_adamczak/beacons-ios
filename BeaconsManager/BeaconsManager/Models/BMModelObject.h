//
//  BMModelObject.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@interface BMModelObject : MTLModel <MTLJSONSerializing>

@property NSDate *createdAt;
@property NSDate *updatedAt;

- (NSURL*)absoluteURLOfPath:(NSString*)path;

@end

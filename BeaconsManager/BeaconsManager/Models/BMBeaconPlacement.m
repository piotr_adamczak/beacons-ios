//
//  BMBeaconPlacement.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMBeaconPlacement.h"
#import "BMSetupManager.h"
#import "BMBeaconManager.h"

@interface BMBeaconPlacement()
@end

@implementation BMBeaconPlacement
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self) {
        
        
        [[BMSetupManager sharedBMSetupManager] fetchMapsWithBlock:^(NSArray *maps) {
            
            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@", self.mapId];
            NSArray * filteredArray = [maps filteredArrayUsingPredicate:predicate];
            
            if (filteredArray.count > 0) {
                self.map = [filteredArray objectAtIndex:0];
            }
        }];
        
        [[BMBeaconManager sharedBMBeaconManager] fetchBeacons:^(NSArray *beacons) {
            
            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@", self.beaconId];
            NSArray * filteredArray = [beacons filteredArrayUsingPredicate:predicate];
            
            if (filteredArray.count > 0) {
                self.beacon = [filteredArray objectAtIndex:0];
            }
        }];        
    }
    return self;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{
             @"uid": @"id",
             @"name": @"name",
             @"mapId": @"map_id",
             @"beaconId": @"beacon_id",
             @"x" : @"x",
             @"y" : @"y",
             @"createdAt" : @"created_at",
             @"updatedAt" : @"updated_at"
             };
}

- (CGPoint)realCenterInMeters {
    return CGPointMake([self.x floatValue] / 100.0f, [self.y floatValue] / 100.0f);
}

- (CGPoint)centerForImage {
  
    CGSize realSize = [self.map realSize];
    CGSize imageSize = [self.map imageSize];
    
    CGFloat relativeX = [self.x floatValue] / realSize.width;
    CGFloat relativeY = [self.y floatValue] / realSize.height;
    
    CGPoint realCenter = CGPointMake(relativeX * imageSize.width, relativeY * imageSize.height);
    return realCenter;
}

- (NSNumber*)radius {
    
    CGSize realSize = [self.map realSize];
    CGSize imageSize = [self.map imageSize];
    
    CGFloat relativeRadius = ([self.realRadius floatValue] * 100) / realSize.width ;
    CGFloat radiusInPixels = relativeRadius * imageSize.width * 0.5;
    return [NSNumber numberWithFloat:radiusInPixels];
}

@end

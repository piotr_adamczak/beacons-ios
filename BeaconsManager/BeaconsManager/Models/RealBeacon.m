//
//  RealBeacon.m
//  BeaconsManager
//
//  Created by Piotrek on 16.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "RealBeacon.h"

@implementation RealBeacon

- (NSString *)fullUUID {
    return [NSString stringWithFormat:@"%@-%@-%@", self.uuid, self.minor, self.major];
}

- (NSUInteger)hash {
    return [[self fullUUID] hash];
}

+ (RealBeacon *)realBeaconFromESTBeacon:(ESTBeacon *)estBeacon {
    
    RealBeacon * realBeacon = [[RealBeacon alloc] init];
    realBeacon.uuid = [estBeacon proximityUUID];
    realBeacon.minor = [estBeacon minor];
    realBeacon.major = [estBeacon major];
    return realBeacon;
}
@end

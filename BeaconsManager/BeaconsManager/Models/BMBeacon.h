//
//  BMBeacon.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMModelObject.h"

@interface BMBeacon : BMModelObject

@property NSNumber *uid;
@property NSString *uuid;
@property NSNumber *major;
@property NSNumber *minor;
@property NSNumber *colorId;

- (NSString*)minorAndMajor;
- (NSString*)fullUUID;

@end

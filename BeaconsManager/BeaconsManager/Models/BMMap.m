//
//  BMMap.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMMap.h"
#import "BMAPIClient.h"
#import "BMSetupManager.h"

#import <AFNetworking/AFImageRequestOperation.h>

@interface BMMap()
@property (nonatomic, assign) CGSize mapSize;
@end

@implementation BMMap

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self) {
        
        
        [[BMSetupManager sharedBMSetupManager] fetchPlacesWithBlock:^(NSArray *places) {
            
            NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uid == %@", self.placeId];
            NSArray * filteredArray = [places filteredArrayUsingPredicate:predicate];
            
            if (filteredArray.count > 0) {
                self.place = [filteredArray objectAtIndex:0];
            }
        }];
        
    }
    return self;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{
             @"uid": @"id",
             @"name": @"name",
             @"url": @"url",
             @"floor": @"minor",
             @"width" : @"width",
             @"height" : @"height",
             @"imageWidth" : @"image_width",
             @"imageHeight" : @"image_height",
             @"placeId" : @"place_id",
             @"createdAt" : @"created_at",
             @"updatedAt" : @"updated_at",
             @"savedMapType" : @"map_type"
             };
}

- (NSURL *)mediaURL {
    return [self absoluteURLOfPath:self.url];
}

- (CGSize)imageSize {
    return CGSizeMake([self.imageWidth floatValue], [self.imageHeight floatValue]);
}

- (CGSize)realSize {
    return CGSizeMake([self.width floatValue], [self.height floatValue]);
}

- (BMMapType)mapType {
    return [self.savedMapType integerValue];
}

@end

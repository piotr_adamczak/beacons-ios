//
//  BMPlace.h
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMModelObject.h"

@interface BMPlace : BMModelObject

@property NSNumber *uid;
@property NSString *name;

@end

//
//  BMModelObject.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMModelObject.h"
#import <Mantle/MTLValueTransformer.h>
#import "BMJSONRequestOperation.h"
#import "BMAPIClient.h"

@implementation BMModelObject

+ (NSValueTransformer *)createdAtJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        return [[BMJSONRequestOperation UTCDate] dateFromString:str];
    } reverseBlock:^(NSDate *date) {
        return [[BMJSONRequestOperation UTCDate] stringFromDate:date];
    }];
}

+ (NSValueTransformer *)updatedAtJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        return [[BMJSONRequestOperation UTCDate] dateFromString:str];
    } reverseBlock:^(NSDate *date) {
        return [[BMJSONRequestOperation UTCDate] stringFromDate:date];
    }];
}

- (NSURL *)absoluteURLOfPath:(NSString *)path {
    NSString * urlString = [NSString stringWithFormat:@"%@%@", kBMBaseURLString, path];
    return [NSURL URLWithString:urlString];
}

@end

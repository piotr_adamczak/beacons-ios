//
//  RealBeacon.h
//  BeaconsManager
//
//  Created by Piotrek on 16.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESTBeacon.h"

@interface RealBeacon : NSObject
@property NSUUID *uuid;
@property NSNumber *minor;
@property NSNumber *major;

+ (RealBeacon*)realBeaconFromESTBeacon:(ESTBeacon*)estBeacon;

@end

//
//  BMPlace.m
//  Praca Magisterska - PP - 94257
//
//  Created by Piotr Adamczak on 15.06.14.
//  Copyright (c) 2014 Piotr Adamczak. All rights reserved.
//

#import "BMPlace.h"

@implementation BMPlace

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{
             @"uid": @"id",
             @"name": @"name",
             @"createdAt" : @"created_at",
             @"updatedAt" : @"updated_at"
             };
}

@end

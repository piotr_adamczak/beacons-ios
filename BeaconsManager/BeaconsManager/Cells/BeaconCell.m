//
//  BeaconCell.m
//  BeaconsManager
//
//  Created by Piotrek on 16.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import "BeaconCell.h"

@interface BeaconCell()
@property (nonatomic, strong) UILabel * majorLabel;
@property (nonatomic, strong) UILabel * minorLabel;
@property (nonatomic, strong) UILabel * proximityLabel;
@property (nonatomic, strong) UILabel * identifierLabel;
@property (nonatomic, strong) UILabel * majorValueLabel;
@property (nonatomic, strong) UILabel * minorValueLabel;
@property (nonatomic, strong) UILabel * proximityValueLabel;
@property (nonatomic, strong) UILabel * identifierValueLabel;
@property (nonatomic, strong) UIImageView * beaconColorView;

@end

@implementation BeaconCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [self setBackgroundColor:[UIColor whiteColor]];
        
        self.majorLabel = [self standardLabel];
        [self.majorLabel setText:NSLocalizedString(@"MAJOR", nil)];
        [self.contentView addSubview:self.majorLabel];
        
        self.minorLabel = [self standardLabel];
        [self.minorLabel setText:NSLocalizedString(@"MINOR", nil)];
        [self.contentView addSubview:self.minorLabel];
        
        self.proximityLabel = [self standardLabel];
        [self.proximityLabel setText:NSLocalizedString(@"PROXIMITY", nil)];
        [self.contentView addSubview:self.proximityLabel];

        self.identifierLabel = [self standardLabel];
        [self.identifierLabel setText:NSLocalizedString(@"IDENTIFIER", nil)];
        [self.identifierLabel setTextAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:self.identifierLabel];

        self.majorValueLabel = [self valueLabel];
        [self.contentView addSubview:self.majorValueLabel];
        
        self.minorValueLabel = [self valueLabel];
        [self.contentView addSubview:self.minorValueLabel];

        self.proximityValueLabel = [self valueLabel];
        [self.contentView addSubview:self.proximityValueLabel];
        
        self.identifierValueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.identifierValueLabel setFont:[UIFont systemFontOfSize:10]];
        [self.identifierValueLabel setTextColor:HTML(0x478c83)];
        [self.identifierValueLabel setBackgroundColor:[UIColor clearColor]];
        [self.identifierValueLabel setTextAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:self.identifierValueLabel];

    }
    return self;
}

- (UILabel*)standardLabel {
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectZero];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    [label setTextColor:HTML(0xb9b2af)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    return label;
}

- (UILabel*)valueLabel {
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectZero];
    [label setFont:[UIFont systemFontOfSize:18]];
    [label setTextColor:[UIColor blackColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    return label;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect majorFrame = CGRectMake(20, 12, 57, 10);
    CGRect majorValueFrame = CGRectOffset(majorFrame, 0, 10);
    majorValueFrame.size.height = 32;
    
    CGRect minorFrame = CGRectOffset(majorFrame, 93, 0);
    CGRect minorValueFrame = CGRectOffset(majorValueFrame, 93, 0);
    
    CGRect proximityFrame = CGRectOffset(minorFrame, 80, 0);
    proximityFrame.size.width = 100;
    CGRect proximityValueFrame = CGRectOffset(minorValueFrame, 80, 0);
    proximityValueFrame.size.width = 95;

    CGRect identifierFrame = CGRectMake(20, CGRectGetMaxY(majorValueFrame) + 4, 100, 10);
    CGRect identifierValueFrame = CGRectOffset(identifierFrame, 0, 15);
    identifierValueFrame.size.height = 13;
    identifierValueFrame.size.width = 300;
    
    [self.majorLabel setFrame:majorFrame];
    [self.majorValueLabel setFrame:majorValueFrame];
    [self.minorLabel setFrame:minorFrame];
    [self.minorValueLabel setFrame:minorValueFrame];
    [self.proximityLabel setFrame:proximityFrame];
    [self.proximityValueLabel setFrame:proximityValueFrame];
    [self.identifierLabel setFrame:identifierFrame];
    [self.identifierValueLabel setFrame:identifierValueFrame];
}

- (void)setMajor:(NSString*)major {
    [self.majorValueLabel setText:major];
}

- (void)setMinor:(NSString*)minor {
    [self.minorValueLabel setText:minor];
}

- (void)setIdentifier:(NSString*)identifier {
    [self.identifierValueLabel setText:identifier];
}

- (void)setProximity:(NSString*)proximity {
    [self.proximityValueLabel setText:proximity];
}

@end

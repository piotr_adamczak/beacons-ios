//
//  BeaconCell.h
//  BeaconsManager
//
//  Created by Piotrek on 16.06.2014.
//  Copyright (c) 2014 PiotrAdamczak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeaconCell : UITableViewCell

- (void)setMajor:(NSString*)major;
- (void)setMinor:(NSString*)minor;
- (void)setIdentifier:(NSString*)identifier;
- (void)setProximity:(NSString*)proximity;

@end
